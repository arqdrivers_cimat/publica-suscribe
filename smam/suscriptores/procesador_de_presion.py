#!/usr/bin/env python
# -*- coding: utf-8 -*-
# -------------------------------------------------------------------------
# Archivo: procesador_de_presion.py
# Capitulo: 3 Estilo Publica-Subscribe
# Autor(es): Perla Velasco & Yonathan Mtz.
# Version: 2.1.0 Marzo 2020
# Descripción:
#
#   Esta clase define el rol de un suscriptor, es decir, es un componente que recibe mensajes.
#
#   Las características de ésta clase son las siguientes:
#
#                                     procesador_de_presion.py
#           +-----------------------+-------------------------+------------------------+
#           |  Nombre del elemento  |     Responsabilidad     |      Propiedades       |
#           +-----------------------+-------------------------+------------------------+
#           |                       |                         |  - Se suscribe a los   |
#           |                       |                         |    eventos generados   |
#           |                       |  - Procesar valores     |    por el wearable     |
#           |     Procesador de     |    extremos de la       |    Xiaomi My Band.     |
#           |        Presión        |    presión arterial.    |  - Define el valor ex- |
#           |                       |                         |    tremo de la presión |
#           |                       |                         |    arterial en 110.    |
#           |                       |                         |  - Notifica al monitor |
#           |                       |                         |    cuando un valor ex- |
#           |                       |                         |    tremo es detectado. |
#           +-----------------------+-------------------------+------------------------+
#
#   A continuación se describen los métodos que se implementaron en ésta clase:
#
#                                               Métodos:
#           +------------------------+--------------------------+-----------------------+
#           |         Nombre         |        Parámetros        |        Función        |
#           +------------------------+--------------------------+-----------------------+
#           |                        |  - ch: propio de Rabbit. |  - Procesa y detecta  |
#           |                        |  - method: propio de     |    valores extremos de|
#           |                        |     Rabbit.              |    la presión         |
#           |       callback()       |  - properties: propio de |    arterial.          |
#           |                        |     Rabbit.              |                       |
#           |                        |  - body: mensaje recibi- |                       |
#           |                        |     do.                  |                       |
#           +------------------------+--------------------------+-----------------------+
#
#
#           Nota: "propio de Rabbit" implica que se utilizan de manera interna para realizar
#            de manera correcta la recepcion de datos, para éste ejemplo no shubo necesidad
#            de utilizarlos y para evitar la sobrecarga de información se han omitido sus
#            detalles. Para más información acerca del funcionamiento interno de RabbitMQ
#            puedes visitar: https://www.rabbitmq.com/
#
# -------------------------------------------------------------------------
from procesador import Procesador
import json
import time
import sys
sys.path.append('../')
from monitor import Monitor


class ProcesadorPresion(Procesador):

    def __init__(self):
        self.queue_name = 'blood_preasure'

    def callback(self, ch, method, properties, body):
        json_message = json.loads(body)
        if int(json_message['blood_preasure']) > 110:
            monitor = Monitor()
            monitor.print_notification(
                json_message['datetime'],
                json_message['id'],
                json_message['blood_preasure'],
                'presión arterial',
                json_message['model'])
        time.sleep(1)
        ch.basic_ack(delivery_tag=method.delivery_tag)


if __name__ == '__main__':
    p_presion = ProcesadorPresion()
    p_presion.consume()
