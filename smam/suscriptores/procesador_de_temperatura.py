#!/usr/bin/env python
# -*- coding: utf-8 -*-
# -------------------------------------------------------------------------
# Archivo: procesador_de_temperatura.py
# Capitulo: 3 Estilo Publica-Subscribe
# Autor(es): Perla Velasco & Yonathan Mtz.
# Version: 2.1.0 Marzo 2020
# Descripción:
#
#   Esta clase define el rol de un suscriptor, es decir, es un componente que recibe mensajes.
#
#   Las características de ésta clase son las siguientes:
#
#                                   procesador_de_temperatura.py
#           +-----------------------+-------------------------+------------------------+
#           |  Nombre del elemento  |     Responsabilidad     |      Propiedades       |
#           +-----------------------+-------------------------+------------------------+
#           |                       |                         |  - Se suscribe a los   |
#           |                       |                         |    eventos generados   |
#           |                       |  - Procesar valores     |    por el wearable     |
#           |     Procesador de     |    extremos de          |    Xiaomi My Band.     |
#           |     Temperatura       |    temperatura.         |  - Define el valor ex- |
#           |                       |                         |    tremo de la         |
#           |                       |                         |    temperatura.        |
#           |                       |                         |  - Notifica al monitor |
#           |                       |                         |    cuando un valor ex- |
#           |                       |                         |    tremo es detectado. |
#           +-----------------------+-------------------------+------------------------+
#
#   A continuación se describen los métodos que se implementaron en ésta clase:
#
#                                               Métodos:
#           +------------------------+--------------------------+-----------------------+
#           |         Nombre         |        Parámetros        |        Función        |
#           +------------------------+--------------------------+-----------------------+
#           |                        |  - ch: propio de Rabbit. |  - Procesa y detecta  |
#           |                        |  - method: propio de     |    valores extremos   |
#           |                        |     Rabbit.              |    de la temperatura. |
#           |       callback()       |  - properties: propio de |                       |
#           |                        |     Rabbit.              |                       |
#           |                        |  - body: mensaje recibi- |                       |
#           |                        |     do.                  |                       |
#           +------------------------+--------------------------+-----------------------+
#
#
#           Nota: "propio de Rabbit" implica que se utilizan de manera interna para realizar
#            de manera correcta la recepcion de datos, para éste ejemplo no shubo necesidad
#            de utilizarlos y para evitar la sobrecarga de información se han omitido sus
#            detalles. Para más información acerca del funcionamiento interno de RabbitMQ
#            puedes visitar: https://www.rabbitmq.com/
#
# -------------------------------------------------------------------------
from procesador import Procesador
import json
import time
import sys
sys.path.append('../')
from monitor import Monitor


class ProcesadorTemperatura(Procesador):

    def __init__(self):
        self.queue_name = 'body_temperature'

    def callback(self, ch, method, properties, body):
        json_message = json.loads(body)
        if float(json_message['body_temperature']) > 69:
            monitor = Monitor()
            monitor.print_notification(
                json_message['datetime'],
                json_message['id'],
                json_message['body_temperature'],
                'temperatura corporal',
                json_message['model'])
        time.sleep(1)
        ch.basic_ack(delivery_tag=method.delivery_tag)


if __name__ == '__main__':
    p_temperatura = ProcesadorTemperatura()
    p_temperatura.consume()
