#!/usr/bin/env python
# -*- coding: utf-8 -*-
# -------------------------------------------------------------------------
# Archivo: procesador_de_aceleracion.py
# Capitulo: 3 Estilo Publica-Subscribe
# Autor(es): Javier Gonzalez, Israel Faustino & Elizabeth Villanueva.
# Version: 1.1.0 Marzo 2020
# Descripción:
#
#   Esta clase define el rol de un suscriptor, es decir, es un componente que recibe mensajes.
#
#   Las características de ésta clase son las siguientes:
#
#                                     procesador_de_aceleracion.py
#           +-----------------------+-------------------------+------------------------+
#           |  Nombre del elemento  |     Responsabilidad     |      Propiedades       |
#           +-----------------------+-------------------------+------------------------+
#           |                       |                         |  - Se suscribe a los   |
#           |                       |                         |    eventos generados   |
#           |                       |  - Procesar valores     |    por el wearable     |
#           |     Procesador de     |    extremos de la       |    Xiaomi My Band.     |
#           |     Aceleración       |    aceleración.         |  - Define el valor ex- |
#           |                       |                         |    tremo en cada uno   |
#           |                       |                         |    de los ejes en ±0.5g|
#           |                       |                         |  - Notifica al monitor |
#           |                       |                         |    cuando un valor ex- |
#           |                       |                         |    tremo es detectado. |
#           +-----------------------+-------------------------+------------------------+
#
#   A continuación se describen los métodos que se implementaron en ésta clase:
#
#                                               Métodos:
#           +------------------------+--------------------------+-----------------------+
#           |         Nombre         |        Parámetros        |        Función        |
#           +------------------------+--------------------------+-----------------------+
#           |                        |  - ch: propio de Rabbit. |  - Procesa y detecta  |
#           |                        |  - method: propio de     |    valores extremos de|
#           |                        |     Rabbit.              |    la aceleración.    |
#           |       callback()       |  - properties: propio de |                       |
#           |                        |     Rabbit.              |                       |
#           |                        |  - body: mensaje recibi- |                       |
#           |                        |     do.                  |                       |
#           +------------------------+--------------------------+-----------------------+
#
#
#           Nota: "propio de Rabbit" implica que se utilizan de manera interna para realizar
#            de manera correcta la recepcion de datos, para éste ejemplo no shubo necesidad
#            de utilizarlos y para evitar la sobrecarga de información se han omitido sus
#            detalles. Para más información acerca del funcionamiento interno de RabbitMQ
#            puedes visitar: https://www.rabbitmq.com/
#
# -------------------------------------------------------------------------
from procesador import Procesador
import json
import time
import sys
sys.path.append('../')
from monitor import Monitor


class ProcesadorAceleracion(Procesador):

    def __init__(self):
        self.queue_name = 'accelerometer'

    def callback(self, ch, method, properties, body):
        json_message = json.loads(body)
        if abs(float(json_message['z_position'])) > 0.5:
            monitor = Monitor()
            monitor.print_notification_fall(
                json_message['datetime'],
                json_message['id'],
                json_message['z_position'],
                'aceleración en z',
                json_message['model'])

        elif abs(float(json_message['y_position'])) > 0.5:
            monitor = Monitor()
            monitor.print_notification_fall(
                json_message['datetime'],
                json_message['id'],
                json_message['y_position'],
                'aceleración en y',
                json_message['model'])

        elif abs(float(json_message['x_position'])) > 0.5:
            monitor = Monitor()
            monitor.print_notification_fall(
                json_message['datetime'],
                json_message['id'],
                json_message['x_position'],
                'aceleración en x',
                json_message['model'])

        time.sleep(1)
        ch.basic_ack(delivery_tag=method.delivery_tag)


if __name__ == '__main__':
    p_aceleracion = ProcesadorAceleracion()
    p_aceleracion.consume()
