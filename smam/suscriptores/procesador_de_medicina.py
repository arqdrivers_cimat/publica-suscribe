#!/usr/bin/env python
# -*- coding: utf-8 -*-
# -------------------------------------------------------------------------
# Archivo: procesador_de_medicina.py
# Capitulo: 3 Estilo Publica-Subscribe
# Autor(es): Javier Gonzalez, Israel Faustino & Elizabeth Villanueva.
# Version: 1.2.0 Marzo 2020
# Descripción:
#
#   Esta clase define el rol de un suscriptor, es decir, es un componente que recibe mensajes.
#
#   Las características de ésta clase son las siguientes:
#
#                                     procesador_de_medicina.py
#           +-----------------------+-------------------------+------------------------+
#           |  Nombre del elemento  |     Responsabilidad     |      Propiedades       |
#           +-----------------------+-------------------------+------------------------+
#           |                       |                         |  - Se suscribe a los   |
#           |                       |                         |    eventos generados   |
#           |                       |  - Procesar mensajes    |    por el wearable     |
#           |     Procesador de     |    de aviso para tomar  |    Xiaomi My Band.     |
#           |     medicina          |    medicina.            |                        |
#           |                       |                         |                        |
#           |                       |                         |                        |
#           |                       |                         |  - Notifica al monitor |
#           |                       |                         |    cuando una          |
#           |                       |                         |    notificación es     |
#           |                       |                         |    recibida.            |
#           +-----------------------+-------------------------+------------------------+
#
#   A continuación se describen los métodos que se implementaron en ésta clase:
#
#                                               Métodos:
#           +------------------------+--------------------------+-----------------------+
#           |         Nombre         |        Parámetros        |        Función        |
#           +------------------------+--------------------------+-----------------------+
#           |                        |  - ch: propio de Rabbit. |  - Procesa y manda    |
#           |                        |  - method: propio de     |    notificación para  |
#           |                        |     Rabbit.              |    la toma de         |
#           |       callback()       |  - properties: propio de |    medicina.          |
#           |                        |     Rabbit.              |                       |
#           |                        |  - body: mensaje recibi- |                       |
#           |                        |     do.                  |                       |
#           +------------------------+--------------------------+-----------------------+
#
#
#           Nota: "propio de Rabbit" implica que se utilizan de manera interna para realizar
#            de manera correcta la recepcion de datos, para éste ejemplo no shubo necesidad
#            de utilizarlos y para evitar la sobrecarga de información se han omitido sus
#            detalles. Para más información acerca del funcionamiento interno de RabbitMQ
#            puedes visitar: https://www.rabbitmq.com/
#
# -------------------------------------------------------------------------
from procesador import Procesador
import json
import time
import sys
sys.path.append('../')
from monitor import Monitor


class ProcesadorMedicina(Procesador):

    def __init__(self):
        self.queue_name = 'medicine'

    def callback(self, ch, method, properties, body):
        json_message = json.loads(body)
        monitor = Monitor()
        monitor.print_notification_medicine(
            json_message['datetime'],
            json_message['id'],
            json_message['medicine'],
            json_message['medicine_via'],
            json_message['medicine_dose'],
            json_message['model'])

        time.sleep(1)
        ch.basic_ack(delivery_tag=method.delivery_tag)


if __name__ == '__main__':
    p_medicina = ProcesadorMedicina()
    p_medicina.consume()
