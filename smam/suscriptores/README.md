# Suscriptores

Esta carpeta contiene los suscriptores del SMAM.

## Versión

3.0.0 - Marzo 2020

## Autores

* **Perla Velasco**
* **Yonathan Martínez**
* **Javier Gonzalez**
* **Israel Israel Faustino**
* **Elizabeth Villanueva**
