#!/usr/bin/env python
# -*- coding: utf-8 -*-
# -------------------------------------------------------------------------
# Archivo: procesador.py
# Capitulo: 3 Estilo Publica-Subscribe
# Autor(es): Javier Gonzalez, Israel Faustino & Elizabeth Villanueva.
# Version: 1.0.0 Marzo 2020
# Descripción:
#
#   Esta interfaz define la estructura general que debe tener un procesador.
#
#
#
#                                     procesador_de_aceleracion.py
#           +-----------------------+-------------------------+------------------------+
#           |  Nombre del elemento  |     Responsabilidad     |      Propiedades       |
#           +-----------------------+-------------------------+------------------------+
#           |                       |                         |  - Se suscribe a los   |
#           |                       |                         |    eventos generados   |
#           |                       |  - Interfaz que define  |    por el wearable     |
#           |     Procesador        |    la estructura general|    Xiaomi My Band.     |
#           |                       |    de clases tipo       |                        |
#           |                       |    procesador.          |                        |
#           |                       |                         |                        |
#           |                       |                         |                        |
#           |                       |                         |                        |
#           |                       |                         |                        |
#           +-----------------------+-------------------------+------------------------+
#
#   A continuación se describen los métodos que se implementaron en ésta clase:
#
#                                               Métodos:
#           +------------------------+--------------------------+-----------------------+
#           |         Nombre         |        Parámetros        |        Función        |
#           +------------------------+--------------------------+-----------------------+
#           |                        |                          |  - Recibe los signos  |
#           |       consume()        |          Ninguno         |    vitales vitales    |
#           |                        |                          |    desde el distribui-|
#           |                        |                          |    dor de mensajes.   |
#           +------------------------+--------------------------+-----------------------+
#           |                        |  - ch: propio de Rabbit. |  - Procesa y detecta  |
#           |                        |  - method: propio de     |    valores            |
#           |                        |     Rabbit.              |    dados por el dis-  |
#           |       callback()       |  - properties: propio de |    tribuidor de       |
#           |                        |     Rabbit.              |    mensajes           |
#           |                        |  - body: mensaje recibi- |                       |
#           |                        |     do.                  |                       |
#           +------------------------+--------------------------+-----------------------+
#
#
#           Nota: "propio de Rabbit" implica que se utilizan de manera interna para realizar
#            de manera correcta la recepcion de datos, para éste ejemplo no shubo necesidad
#            de utilizarlos y para evitar la sobrecarga de información se han omitido sus
#            detalles. Para más información acerca del funcionamiento interno de RabbitMQ
#            puedes visitar: https://www.rabbitmq.com/
#
# -------------------------------------------------------------------------
import pika
import sys
import time

from abc import ABC, abstractmethod


class Procesador(ABC):

    def consume(self):
        try:
            # Se establece la conexión con el Distribuidor de Mensajes
            connection = pika.BlockingConnection(
                pika.ConnectionParameters(host='localhost'))
            # Se solicita un canal por el cuál se enviarán los signos vitales
            channel = connection.channel()
            # Se declara una cola para leer los mensajes enviados por el
            # Publicador
            channel.queue_declare(queue=self.queue_name, durable=True)
            channel.basic_qos(prefetch_count=1)
            channel.basic_consume(
                on_message_callback=self.callback,
                queue=self.queue_name)
            channel.start_consuming()  # Se realiza la suscripción en el Distribuidor de Mensajes
        except (KeyboardInterrupt, SystemExit):
            channel.close()  # Se cierra la conexión
            sys.exit("Conexión finalizada...")
            time.sleep(1)
            sys.exit("Programa terminado...")

    @abstractmethod
    def callback(self, ch, method, properties, body):
        pass


if __name__ == '__main__':
    pass
