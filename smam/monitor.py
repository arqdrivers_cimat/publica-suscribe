#!/usr/bin/env python
# -*- coding: utf-8 -*-
# -------------------------------------------------------------------------
# Archivo: monitor.py
# Capitulo: 3 Estilo Publica-Subscribe
# Autor(es): Perla Velasco & Yonathan Mtz.
# Version: 3.1.1 Marzo 2020
# Descripción:
#
#   Ésta clase define el rol del monitor, es decir, muestra datos, alertas y advertencias sobre los signos vitales de los adultos mayores.
#
#   Las características de ésta clase son las siguientes:
#
#                                            monitor.py
#           +-----------------------+-------------------------+------------------------+
#           |  Nombre del elemento  |     Responsabilidad     |      Propiedades       |
#           +-----------------------+-------------------------+------------------------+
#           |        Monitor        |  - Mostrar datos a los  |         Ninguna        |
#           |                       |    usuarios finales.    |                        |
#           +-----------------------+-------------------------+------------------------+
#
#   A continuación se describen los métodos que se implementaron en ésta clase:
#
#                                             Métodos:
#           +------------------------+--------------------------+-----------------------+
#           |         Nombre         |        Parámetros        |        Función        |
#           +------------------------+--------------------------+-----------------------+
#           |  print_notification()  |  - datetime: fecha en que|  - Imprime el mensa-  |
#           |                        |     se envió el mensaje. |    je recibido.       |
#           |                        |  - id: identificador del |                       |
#           |                        |     dispositivo que      |                       |
#           |                        |     envió el mensaje.    |                       |
#           |                        |  - value: valor extremo  |                       |
#           |                        |     que se desea notifi- |                       |
#           |                        |     car.                 |                       |
#           |                        |  - name_param: signo vi- |                       |
#           |                        |     tal que se desea no- |                       |
#           |                        |  - model: modelo del     |                       |
#           |                        |     wearable             |                       |
#           |                        |     car.                 |                       |
#           |                        |  - additional_text:      |                       |
#           |                        |     texto con mensaje    |                       |
#           +------------------------+--------------------------+-----------------------+
#           |                        |     adiciónal.           |                       |
#           |  print_notification_   |  - datetime: fecha en que|  - Imprime el mensa-  |
#           |  fall()                |     se envió el mensaje. |    je recibido más.   |
#           |                        |  - id: identificador del |    un mensaje de      |
#           |                        |     dispositivo que      |    alerta de caida.   |
#           |                        |     envió el mensaje.    |                       |
#           |                        |  - value: valor extremo  |                       |
#           |                        |     que se desea notifi- |                       |
#           |                        |     car.                 |                       |
#           |                        |  - name_param: signo vi- |                       |
#           |                        |     tal que se desea no- |                       |
#           |                        |  - model: modelo del     |                       |
#           |                        |     wearable             |                       |
#           |                        |     car.                 |                       |
#           +------------------------+--------------------------+-----------------------+
#           |  print_notification_   |  - datetime: fecha en que|  - Imprime el mensa-  |
#           |  medicine()            |     se envió el mensaje. |    je sobre cual      |
#           |                        |  - id: identificador del |    medicina a         |
#           |                        |     dispositivo que      |    administrar.       |
#           |                        |     envió el mensaje.    |                       |
#           |                        |  - value: valor extremo  |                       |
#           |                        |     que se desea notifi- |                       |
#           |                        |     car.                 |                       |
#           |                        |  - medicine: medicina    |                       |
#           |                        |      a administrar       |                       |
#           |                        |  - model: modelo del     |                       |
#           |                        |     wearable             |                       |
#           |                        |     car.                 |                       |
#           +------------------------+--------------------------+-----------------------+
#           |   format_datetime()    |  - datetime: fecha que se|  - Formatea la fecha  |
#           |                        |     formateará.          |    en que se recibió  |
#           |                        |                          |    el mensaje.        |
#           +------------------------+--------------------------+-----------------------+
#
# -------------------------------------------------------------------------


class Monitor:

    def __init__(self):
        self.alert_text = []
        self.alert_text.append(
            "  ---------------------------------------------------")
        self.alert_text.append("    ADVERTENCIA")
        self.alert_text.append(
            "  ---------------------------------------------------")

    def print_notification(
            self,
            datetime,
            id,
            value,
            name_param,
            model,
            additional_text=""):
        print(self.alert_text[0])
        print(self.alert_text[1])
        print(self.alert_text[2])
        print("    Se ha detectado un incremento de " +
              str(name_param) +
              " (" +
              str(value) +
              ")" +
              " a las " +
              str(self.format_datetime(datetime)) +
              " en el adulto mayor que utiliza el dispositivo " +
              str(model) +
              ":" +
              str(id))
        print(additional_text)
        print("")
        print("")

    def print_notification_fall(self, datetime, id, value, name_param, model):
        self.print_notification(datetime, id, value, name_param, model,
                                "    El adulto mayor pudo sufrir una caida.")

    def print_notification_medicine(
            self,
            datetime,
            id,
            medicine,
            via,
            dose,
            model):
        print(self.alert_text[0])
        print(self.alert_text[1])
        print(self.alert_text[2])
        print(
            "    Favor de administrar " +
            str(dose) +
            " " +
            via +
            " de " +
            medicine +
            " al adulto mayor que utiliza el dispositivo " +
            str(model) +
            ":" +
            str(id))
        print("    " + str(self.format_datetime(datetime)))
        print("")
        print("")

    def format_datetime(self, datetime):
        values_datetime = datetime.split(':')
        f_datetime = values_datetime[3] + ":" + values_datetime[4] + " del " + \
            values_datetime[0] + "/" + \
            values_datetime[1] + "/" + values_datetime[2]
        return f_datetime
