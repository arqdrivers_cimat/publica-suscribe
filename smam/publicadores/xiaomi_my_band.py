#!/usr/bin/env python
# -*- coding: utf-8 -*-

# -------------------------------------------------------------------------
# Archivo: xiaomi_my_band.py
# Capitulo: 3 Patrón Publica-Subscribe
# Autor(es): Perla Velasco & Yonathan Mtz.
# Version: 1.1.0 Marzo 2020
# Descripción:
#
#   Ésta clase define el rol de un publicador, es decir, es un componente que envia mensajes.
#
#   Las características de ésta clase son las siguientes:
#
#                                       xiaomi_my_band.py
#           +-----------------------+-------------------------+------------------------+
#           |  Nombre del elemento  |     Responsabilidad     |      Propiedades       |
#           +-----------------------+-------------------------+------------------------+
#           |                       |  - Enviar mensajes      |  - Simula información  |
#           |      Publicador       |                         |    sobre algunos signos|
#           |                       |                         |    vitales.            |
#           +-----------------------+-------------------------+------------------------+
#
#   A continuación se describen los métodos que se implementaron en ésta clase:
#
#                                             Métodos:
#           +-----------------------------+--------------------------+-----------------------+
#           |         Nombre              |        Parámetros        |        Función        |
#           +-----------------------------+--------------------------+-----------------------+
#           |                             |                          |  - Inicializa el      |
#           |       __init__()            |          int: id         |    wearable indicando |
#           |                             |                          |    su identificador.  |
#           |                             |                          |  - Inicializa las     |
#           |                             |                          |    posibles medicinas |
#           |                             |                          |    que se pueden tomar|
#           +-----------------------------+--------------------------+-----------------------+
#           |                             |                          |  - Envía los signos   |
#           |        publish()            |          Ninguno         |    vitales al distri- |
#           |                             |                          |    buidor de mensajes.|
#           +-----------------------------+--------------------------+-----------------------+
#           |   simulate_datetime()       |          Ninguno         |  - Simula valores de  |
#           |                             |                          |    fecha y hora.      |
#           +-----------------------------+--------------------------+-----------------------+
#           |                             |          Ninguno         |  - Simula el valor de |
#           |  simulate_x_position()      |                          |    la aceleración en  |
#           |                             |                          |    eje x.             |
#           +-----------------------------+--------------------------+-----------------------+
#           |                             |                          |  - Simula el valor de |
#           |  simulate_y_position()      |          Ninguno         |    la aceleración en  |
#           |                             |                          |    eje y.             |
#           +-----------------------------+--------------------------+-----------------------+
#           |                             |                          |  - Simula el valor de |
#           |  simulate_z_position()      |          Ninguno         |    la aceleración en  |
#           |                             |                          |    eje z.             |
#           +-----------------------------+--------------------------+-----------------------+
#           |                             |                          |  - Simula el valor de |
#           | simulate_body_temperature() |          Ninguno         |    la temperatura     |
#           |                             |                          |    corporal.          |
#           +-----------------------------+--------------------------+-----------------------+
#           |                             |                          |  - Simula los pasos   |
#           |    simulate_step_count()    |          Ninguno         |    dados por un       |
#           |                             |                          |    adulto.            |
#           +-----------------------------+--------------------------+-----------------------+
#           |                             |                          |  - Simula la bateria  |
#           |  simulate_battery_level()   |          Ninguno         |    restante del       |
#           |                             |                          |    wearable.          |
#           +-----------------------------+--------------------------+-----------------------+
#           |                             |                          |  - Simula las horas   |
#           | simulate_hours_of_sleep()   |          Ninguno         |    sueño acumuladas   |
#           |                             |                          |    por un adulto.     |
#           +-----------------------------+--------------------------+-----------------------+
#           |                             |                          |  - Simula las calorias|
#           | simulate_calories_burned()  |          Ninguno         |    consumidas por un  |
#           |                             |                          |    adulto en un día.  |
#           +-----------------------------+--------------------------+-----------------------+
#           |                             |                          |  - Simula el ritmo    |
#           |    simulate_heart_rate()    |          Ninguno         |    cardiaco del cora- |
#           |                             |                          |    zón.               |
#           +-----------------------------+--------------------------+-----------------------+
#           |                             |          Ninguno         |  - Simula la presión  |
#           | simulate_blood_preasure()   |                          |    arterial.          |
#           +-----------------------------+--------------------------+-----------------------+
#           | create_message()            |          Ninguno         |  - Crea un dict con   |
#           |                             |                          |    info del wearable  |
#           +-----------------------------+--------------------------+-----------------------+
#           | publish_message()           |      dict: message       |  - Envia un dict en   |
#           |                             |      str:  queue_name    |  forma json al broker.|
#           +-----------------------------+--------------------------+-----------------------+
#           |                             |                          |  - Simula la selección|
#           |    simulate_medicine()      |          Ninguno         |    , via y dosis de   |
#           |                             |                          |    medicina a tomar.  |
#           |                             |                          |                       |
#           +-----------------------------+--------------------------+-----------------------+
#
# -------------------------------------------------------------------------
import pika
import random
import time
import json


class XiaomiMyBand:
    producer = "Xiaomi"
    model = "Xiaomi My Band 2"
    hardware_version = "2.0.3.2.1"
    software_version = "10.2.3.1"
    step_count = 0
    battery_level = 81
    id = 0

    def __init__(self, id):
        self.medicine = {
            "paracetamol": "pastilla(s)",
            "ibuprofeno": "pastilla(s)",
            "insulina": "inyección",
            "furosemida": "pastilla(s)",
            "piroxicam": "pastilla(s)",
            "tolbutamida": "pastilla(s)"}
        self.id = id

    def create_message(self):
        message = {}
        message['datetime'] = self.simulate_datetime()
        message['producer'] = self.producer
        message['model'] = self.model
        message['hardware_version'] = self.hardware_version
        message['software_version'] = self.software_version
        message['id'] = str(self.id)
        return message

    def publish_message(self, message, queue_name):
        # Se establece la conexión con el Distribuidor de Mensajes
        connection = pika.BlockingConnection(
            pika.ConnectionParameters(host='localhost'))
        # Se solicita un canal por el cuál se enviarán los signos vitales
        channel = connection.channel()
        # Se declara una cola para persistir los mensajes enviados
        channel.queue_declare(queue=queue_name, durable=True)
        channel.basic_publish(
            exchange='',
            routing_key=queue_name,
            body=json.dumps(message),
            properties=pika.BasicProperties(
                delivery_mode=2,
            ))
        connection.close()  # Se cierra la conexión

        time.sleep(1)

    def publish(self):
        message = self.create_message()
        message['body_temperature'] = self.simulate_body_temperature()
        self.publish_message(message, 'body_temperature')

        message = self.create_message()
        message['heart_rate'] = self.simulate_heart_rate()
        self.publish_message(message, 'heart_rate')

        message = self.create_message()
        message['blood_preasure'] = self.simulate_blood_preasure()
        self.publish_message(message, 'blood_preasure')

        message = self.create_message()
        message['z_position'] = self.simulate_z_position()
        message['y_position'] = self.simulate_y_position()
        message['x_position'] = self.simulate_x_position()
        self.publish_message(message, 'accelerometer')

        if random.randint(0, 1) == 1:
            message = self.create_message()
            message["medicine"], message["medicine_via"], message["medicine_dose"] =\
                self.simulate_medicine()
            self.publish_message(message, 'medicine')

    def simulate_datetime(self):
        return time.strftime("%d:%m:%Y:%H:%M:%S")

    def simulate_x_position(self):
        return random.uniform(-1, 1)

    def simulate_y_position(self):
        return random.uniform(-1, 1)

    def simulate_z_position(self):
        return random.uniform(-1, 1)

    def simulate_body_temperature(self):
        return random.uniform(67, 72)

    def simulate_step_count(self):
        self.step_count += 1
        return self.step_count

    def simulate_battery_level(self):
        self.battery_level -= 1
        return self.battery_level

    def simulate_hours_of_sleep(self):
        hours_sleep = 10 - random.uniform(0, 3)
        return hours_sleep

    def simulate_calories_burned(self):
        return random.randint(1500, 2500)

    def simulate_heart_rate(self):
        return random.randint(60, 150)

    def simulate_blood_preasure(self):
        return random.randint(100, 200)

    def simulate_medicine(self):
        medicine = random.choice(list(self.medicine))
        via = self.medicine[medicine]
        dose = random.randint(1, 2)
        return medicine, via, dose
