# Publicadores

Esta carpeta contiene los publicadores del SMAM. Los publicadores simulan, a través de software, los dispositivos 'wearables' Xiaomi My Band que permiten monitorear algunos signos vitales de los adultos mayores.

## Versión

3.0.0 - Marzo 2020

## Autores

* **Perla Velasco**
* **Yonathan Martínez**
